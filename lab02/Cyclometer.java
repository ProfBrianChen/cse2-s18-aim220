//Ana Isabela Madrigal
//Friday, February 2, 2018
//CSE002
//This program is meant to record the data from a bicycle cyclometer which measures speed and distance traveled 
//For two trips, the program should print the duration of each trip, the rotation counts, the distance traveled in each trip and the total distance of both trips combined

public class Cyclometer {
  //main method required for every Java program
  public static void main (String[] args) {
    
 

int T1sec=480; //duration of the first trip in seconds
int T2sec=3220; //duration of secind trip in seconds
int T1counts=1561; //number of rotations of the front wheel for the first trip
int T2counts=9037; //number of rotations of the front wheel for the second trip

double wheelDiameter=27.0,
PI=3.14159,
feetPerMile=5280,
inchesPerFoot=12,
secondsPerMinute=60; 
double T1distance, T2distance, totalDistance;

    
//run the calculations and print out a statement detailing the duration and rotation counts of each trip
System.out.println("Trip 1 took " + (T1sec/secondsPerMinute) + " minutes and had " + T1counts + " counts.");
System.out.println("Trip 2 took " + (T2sec/secondsPerMinute) + " minutes and had " +T2counts + " counts.");  
  
    
T1distance=T1counts*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels the diameter in inches times PI)
T1distance/=inchesPerFoot*feetPerMile; // Gives distance in miles	
T2distance=T2counts*wheelDiameter*PI/inchesPerFoot/feetPerMile;
totalDistance=T1distance+T2distance;

//print out data     
System.out.println("Trip 1 was "+T1distance+" miles");
System.out.println("Trip 2 was "+T2distance+" miles");
System.out.println("The total distance was "+totalDistance+" miles");

  
   } //end of main method
} //end of class
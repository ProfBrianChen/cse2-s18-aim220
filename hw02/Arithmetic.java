//Ana Isabela Madrigal
//Homework 2: Arithmetic
//Tuesday, February 6, 2018
//CSE002
//This program is meant to compute the total cost of items purchased at a store, including the 6% PA sales tax

//class
public class Arithmetic {
  
//main method required for every java program 
  public static void main(String [] args){
    
    //values
    int pants=3; //number of pairs of pants purchased
    int shirts=2; //number of shirts purchased
    int belts=1; //number of belts purchased
    double priceP=34.98; //price per pair of pants
    double priceS=24.99; //price per shirt
    double priceB=33.99; //price per belt
    double paSalesTax=0.06; //Pennsylvania state sales tax 
    
   
    //declaration of variables
    double pantsTotal, shirtsTotal, beltsTotal, 
    pantsTotalTax, shirtsTotalTax, beltsTotalTax, 
    purchaseTotalNoTax, taxTotal, purchaseTotalWithTax;
    
    
    //assignment of values to variables 
    pantsTotal=pants*priceP; //total price of 3 pairs of pants before tax
    shirtsTotal=shirts*priceS; //total price of 2 shirts before tax
    beltsTotal=belts*priceB; //total price of 1 belt before tax

    pantsTotalTax=(paSalesTax*pantsTotal); //total tax paid for 3 pairs of pants 
    shirtsTotalTax=(paSalesTax*shirtsTotal); //total tax paid for 2 shirts 
    beltsTotalTax=(paSalesTax*beltsTotal); //total tax paid for 1 belt 
    
    purchaseTotalNoTax=pantsTotal+shirtsTotal+beltsTotal; //price of total purchase before tax
    taxTotal=pantsTotalTax+shirtsTotalTax+beltsTotalTax; //total sales tax paid for all items
    purchaseTotalWithTax=purchaseTotalNoTax+taxTotal; //total purchase price with sales tax
    
    
    //computations and receipt
    System.out.println("The total cost of pants was $" + pantsTotal); //print the cost of 3 pairs of pants
    System.out.println("The total cost of shirts was $" + shirtsTotal); //print the cost of 2 shirts
    System.out.println("The total cost of belts was $" + beltsTotal); //print the cost of 1 belt
   
    
    System.out.println("The total sales tax on pants was $" + (int) (pantsTotalTax*100)/100.0); //print the tax paid for 3 pairs of pants
    System.out.println("The total sales tax on shirts was $" + (int) (shirtsTotalTax*100)/100.0); //print the tax paid for 2 shirts
    System.out.println("The total sales tax on belts was $" + (int) (beltsTotalTax*100)/100.0); //print the tax paid for 1 belt

    
    System.out.println("The total cost of the purchase before tax was $" + (int) (purchaseTotalNoTax*100)/100.0); //print the cost of total purchase before tax
    System.out.println("The total sales tax for the purchase was $" + (int) (taxTotal*100)/100.0); //print the total sales tax paid for all items
    System.out.println("The total cost of the purchase with tax was $" + (int) (purchaseTotalWithTax*100)/100.0); //print the total cost of the purchase with sales tax
  
  }//end of main method
  
}//end of class
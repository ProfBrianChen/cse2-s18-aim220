//Ana Isabela Madrigal
//Homework 9: Draw Poker
//CSE002
//Tuesday, April 9, 2018
//This code is meant to





public class DrawPoker{
  
  //main method
  public static void main(String[] args){
   
    //initialize arrays
   int[] deck = new int [52];
   int[] playerOne = new int [5];
   int[] playerTwo = new int [5];
   int[] copyArray = new int [5];
   int[] copyArrayTwo = new int [5];
   
   //assign numbers to each array spot 
   for (int i = 0; i < 52; i++){
     deck[i] = i;
   }//end for loop
   
   shuffle(deck);
   deals(deck, playerOne, playerTwo);
   copy(playerOne, playerTwo, copyArray, copyArrayTwo);
   boolean pairOne = pair(copyArray);
   boolean pairTwo = pairTwo(copyArrayTwo);
   boolean twoPairOne = twoPair(copyArray);
   boolean twoPairTwo = twoPairTwo(copyArrayTwo);
   boolean threeOne = threeOfAKind(copyArray);
   boolean threeTwo = threeOfAKindTwo(copyArrayTwo);
   boolean flushOne = flushOne(playerOne);
   boolean flushTwo = flushTwo(playerTwo);
   boolean fullHouse = fullHouse(copyArray);
   boolean fullHouseTwo = fullHouseTwo(copyArrayTwo);
  
   //winner will be determined by a point system 
   //a pair will merit 1 point
   //two pairs will merit 2 points
   //a three of a kind will merit 3 points
   //a flush will merit 4 points
   //a full house will merit 5 points
   
   int pointsPlayerOne = 0;
   int pointsPlayerTwo = 0;
   
   if(pairOne == true){
     System.out.println("Player One has a pair.");
     pointsPlayerOne = 1;
   }
   
   if (pairTwo == true){
     System.out.println("Player Two has a pair.");
     pointsPlayerTwo = 1;
   }
   
   if (twoPairOne == true){
     System.out.println("Player One has two pairs.");
     pointsPlayerOne = 2;
   }
   
   if(twoPairTwo == true){
     System.out.println("Player Two has two pairs.");
     pointsPlayerTwo = 2;
   }
   
   if (threeOne == true){
     System.out.println("Player One has a three of a kind.");
     pointsPlayerOne = 3;
   }
   
    if (threeTwo== true){
     System.out.println("Player Two has a three of a kind.");
     pointsPlayerTwo = 3;
   }
    
    if (flushOne == true){
      System.out.println("Player One has a flush.");
      pointsPlayerOne = 4;
    }
    
     if (flushTwo == true){
      System.out.println("Player Two has a flush.");
      pointsPlayerTwo = 4;
    }
   
    if (fullHouse == true){
     System.out.println("Player One has a full house.");
     pointsPlayerOne = 5;
   }
   
    if (fullHouseTwo == true){
     System.out.println("Player Two has a full house.");
     pointsPlayerTwo = 5;
   }
   
   
   
   if (pointsPlayerOne > pointsPlayerTwo){
     System.out.println("PLAYER ONE WINS!!!");
   }
   
   if (pointsPlayerOne < pointsPlayerTwo){
     System.out.println("PLAYER TWO WINS!!!");
   }
   
   if (pointsPlayerOne == pointsPlayerTwo){
     System.out.println("IT'S A TIE! ");
   }
  
   
 }//end main method
  
  //shuffles cards
  public static void shuffle(int[] shuffledCards){
    
    for (int i = 0; i < 52; i++){
      
      int target = (int) (Math.random() * 52);
      
      int temp = shuffledCards[target];
      shuffledCards[target] = shuffledCards[i];
      shuffledCards[i] = temp;
       
    }//end for loop
    
    System.out.println("The cards are shuffled! We're ready to play!");
  }//end method shuffle
   
  //deals cards by alternatinf between players
  public static void deals(int[] deck, int[] handOne, int[] handTwo){
  
    int counter = 0;
    for (int i = 0; i < 5; i++){
      
      handOne[i] = deck[i + counter];
      counter++;
      System.out.println("P1 Card " + (i+1) + ": " + handOne[i]);
      
      handTwo[i] = deck[i + counter];
      
      System.out.println("P2 Card " + (i+1) + ": " + handTwo[i]);
      System.out.println("");
      
      
    }//end for loop
    
    
    assignment(handOne, handTwo);
    
      
    }//end method deals
   
  //assigns card values to array numbers
  public static void assignment(int[] handOne, int []handTwo){
    
    String suit = "";
    String suitTwo = "";
    for(int i = 0; i < 5; i++){
    
    
      switch((int) handOne[i]/13){
        case 0:
          suit = "hearts";
          break;
        case 1:
          suit = "diamonds";
          break;
        case 2:
          suit = "spades";
          break;
        case 3:
          suit = "clubs";
          break;
      
      }//end switch
      
      
       switch((int) handTwo[i]/13){
        case 0:
          suitTwo = "hearts";
          break;
        case 1:
          suitTwo = "diamonds";
          break;
        case 2:
          suitTwo = "spades";
          break;
        case 3:
          suitTwo = "clubs";
          break;
      
      }//end switch

      
      int val = handOne[i]%13;
      String highCard = "" + (val + 1);
      
      switch(val){
        case 0:
          highCard = "Ace";
          break;
        case 10:
          highCard = "Jack";
          break;
        case 11:
          highCard = "Queen";
          break;
        case 12:
          highCard = "King";
          break;
          
      }//end switch
      
      
      
      
       
      
       int valTwo = handTwo[i]%13;
      String highCardTwo = "" + (valTwo + 1);
      
      switch(valTwo){
        case 0:
          highCardTwo = "Ace";
          break;
        case 10:
          highCardTwo = "Jack";
          break;
        case 11:
          highCardTwo = "Queen";
          break;
        case 12:
          highCardTwo = "King";
          break;
          
      }//end switch
      
       System.out.println("P1 Card " + (i + 1) + ": " + highCard + " of " + suit);
      System.out.println("P2 Card " + (i + 1) + ": " + highCardTwo + " of " + suitTwo);

}//end for loop  
  }//end method assignment  
  
  //makes a copy in order to determine matching card numbers
  public static void copy(int[] originalOne, int [] originalTwo, int[] copyOne, int[] copyTwo){
    
    for(int i=0; i<5; i++){
      copyOne[i] = originalOne[i] % 13;
    }//end for loop
    
    for(int j=0; j<5; j++){
      copyTwo[j] = originalTwo[j] % 13;
    }//end for loop
    
    
    
  }//end method copy
  
  //determines if player one has a pair
  public static boolean pair(int[] firstHand){
    
    int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(firstHand[j] == firstHand[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
      
      
    }//end for loop
    
    System.out.println("Pairs in first hand: " + pairs);
    
    
    if(pairs == 1){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
    
  }//end method pair
 
  //determines if player two has a pair
  public static boolean pairTwo(int[] secondHand){
    
    int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(secondHand[j] == secondHand[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
    
      
    }//end for loop
    
      System.out.println("Pairs in second hand: " + pairs);
     
    if(pairs == 1){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
    
  }//end method pairTwo
  
  //determines if player one has two pairs
  public static boolean twoPair(int[] firstDouble){
     int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(firstDouble[j] == firstDouble[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
      
      
    }//end for loop
    
   // System.out.println("Pairs in first Hand: " + pairs);
    
    
    if(pairs == 2){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
  }//end method twoPair
 
  //determines if player two has two pairs
  public static boolean twoPairTwo(int[] secondDouble){
     int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(secondDouble[j] == secondDouble[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
      
      
    }//end for loop
    
   // System.out.println("Pairs in first Hand: " + pairs);
    
    
    if(pairs == 2){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
  }//end method twoPairTwo
  
  //determines if player one has a three of a kind
  public static boolean threeOfAKind(int[] threeOne){
   
    int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(threeOne[j] == threeOne[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
      
      
    }//end for loop
    
  //  System.out.println("Matches in first Hand: " + pairs);
    
    
    if(pairs == 3){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
    
    
  }//end method threeOfAKind
 
  //determines if player two has a three of a kind
  public static boolean threeOfAKindTwo(int[] threeTwo){
    
    int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(threeTwo[j] == threeTwo[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
      
      
    }//end for loop
    
  //  System.out.println("Matches in first Hand: " + pairs);
    
    
    if(pairs == 3){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
    
  }//end method threeOfAKindTwo
  
  //determines if player one has a flush
  public static boolean flushOne(int[] sameSuit){
    
    int suitOne = (int) sameSuit[0]/13;
    int suitTwo = (int)sameSuit[1]/13;
    int suitThree = (int)sameSuit[2]/13;
    int suitFour = (int)sameSuit[3]/13;
    int suitFive = (int)sameSuit[4]/13;
    
    if (suitOne == suitTwo & suitTwo == suitThree & suitThree == suitFour & suitFour == suitFive){
      return true;
    }
    
    else{
      return false;
    }
    
  }//end method flushOne
  
  //determines if player two has a flush
  public static boolean flushTwo(int[] sameSuitTwo){
    
    int suitOne = (int)sameSuitTwo[0]/13;
    int suitTwo = (int)sameSuitTwo[1]/13;
    int suitThree = (int)sameSuitTwo[2]/13;
    int suitFour = (int)sameSuitTwo[3]/13;
    int suitFive = (int)sameSuitTwo[4]/13;
    
    if (suitOne == suitTwo & suitTwo == suitThree & suitThree == suitFour & suitFour == suitFive){
      return true;
    }
    
    else{
      return false;
    }
    
  }//end method flushTwo
  
  //determines if player one has a full house
  public static boolean fullHouse(int[] fullOne){
    
    int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(fullOne[j] == fullOne[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
      
      
    }//end for loop
    
  //  System.out.println("Matches in first Hand: " + pairs);
    
    
    if(pairs == 4){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
    
  
  }//end method full house

  //determines if player two has a full house
  public static boolean fullHouseTwo(int[] fullTwo){
    
    int pairs = 0;
    for (int i=0; i<5; i++){
      
      for (int j=0; j<i; j++){
        
        if(fullTwo[j] == fullTwo[i]){
          
          pairs++;
          
        }//end if
      }//end nested for loop
      
      
    }//end for loop
    
   // System.out.println("Pairs in first Hand: " + pairs);
    
    
    if(pairs == 4){
      return true;
      
    }//end if
    
    else{
      return false;
    }//end else
    
  }//end method fullHouseTwo

  
  

}//end class
  
  
  
  
  
  
  

  
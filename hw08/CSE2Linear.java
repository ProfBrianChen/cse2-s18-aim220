//Ana Isabela Madrigal
//Homework 8: Linear
//CSE002
//Tuesday, April 9, 2018
//This code is meant to


import java.util.Scanner;
//import java.util.Random;

public class CSE2Linear{
  
  
  public static void findGrade(int [] grades){
    Scanner myScanner = new Scanner (System.in);
    
    
    System.out.print("Enter a specific grade you would like to look for: ");
    
    
    int selected = myScanner.nextInt();
    int arrayPosition = 7;
    int iterationChanges = 4;
    
    if( selected >= grades[0] & selected <= grades[14]){
    for (int i=0; i < 4; i++){
     
//System.out.println(grades[arrayPosition]);
  //    System.out.println(i);
      
      
     if(selected < grades[arrayPosition]){
        
        arrayPosition -= iterationChanges;
        iterationChanges -= (iterationChanges/2);
        continue;
        
      }
      
      
      if (selected > grades[arrayPosition]){
        
        arrayPosition += iterationChanges;
        iterationChanges -= (iterationChanges/2);
        continue;
        
      }
      
      
      if(selected == grades[arrayPosition]){
        i += 1;
        System.out.println("The grade " + selected + " was found in " + i + " iteration(s)");
        break;
        
      }
      
      
      
    }
   
    
     if (selected != grades[arrayPosition]){
        
        System.out.println("The grade " + selected + " was not found in 4 iterations.");
        
        
        
      }
      
        }
        
        
      }
      
    
    
public static void scramble (int [] scrambledGrades){
  
  System.out.println("Let's scramble the grades!");
  
  
  for (int i=0; i<15; i++) {
	//find a random member to swap with
	int target = (int) ( Math.random() *15); 
	


	//swap the values
	int temp = scrambledGrades[target];
	scrambledGrades[target] = scrambledGrades[i];
	scrambledGrades[i] = temp;

   // System.out.println(scrambledGrades);
    
 // System.out.println("");
  }
  
   for (int j = 0; j < 15; j++){
    System.out.print(scrambledGrades[j] + " ");
  }
 System.out.println("");
    
  
      findLinear(scrambledGrades);
      
    
}  
    
  
  
  public static void findLinear(int [] linearSearch){
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Enter a specific grade you would like to look for: ");
    
    int i = 0;
    int input = myScanner.nextInt();
    
    for ( ; i < 15; i++){
      
      if (linearSearch[i] == input){
        
        System.out.println("The grade " + input + " was found in " + (i+1) + " iterations.");
        return;
        
      }
        
    }
    
    
      System.out.println("The grade was not found");
     
      
    
    
    
    
  }
  
    
    
    
  
  
  
  
  
  
  public static void main(String[] args){
    
  //Random randomGenerator = new Random();  
  //int randomArraySize = randomGenerator.nextInt(5) + 5;
  Scanner myScanner = new Scanner(System.in);  
    
  int [] grades = new int [15];
    
  System.out.println("Enter 15 final grades as whole numbers in ascending order. ");
  
    
  for(int i = 0; i < 15; i++){  
    System.out.print(i + 1 + ". ");
    boolean confirmInteger = myScanner.hasNextInt();
    
    if(confirmInteger == true){
    grades[i] = myScanner.nextInt(); 
      
      if(grades[i] < 0 | grades[i] > 100){
        System.out.println("You entered a number outside the possible range. Please try again.");
        i -= 1;
      }
      
      if (i > 0){
       if(grades[i] < grades[i-1]){
        System.out.println("Your input is not in ascending order. Please try again.");
        i -= 1;
      }
      }
    }
    
    else if (confirmInteger == false){
      String junk = myScanner.next();
      System.out.println("You did not enter a whole number. Please try again.");
      i -= 1;
    }
    
 
  }
    
    System.out.println("");
    
   for (int j = 0; j < 15; j++){
    System.out.print(grades[j] + " ");
  }
 System.out.println("");
    
    
    findGrade(grades);
    scramble(grades);
    
    
  }//end of main method
}//end of class
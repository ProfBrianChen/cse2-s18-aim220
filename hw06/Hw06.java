//Ana Isabela Madrigal
//Homework 6: Loops
//CSE002
//Tuesday, March 6, 2018
//This code is meant to read user input and check that the input matches the necessary type
//If the input does not match the type, it should be discarded and the user should be asked to re-enter the information


//import Scanner class
import java.util.Scanner;

public class Hw06 {
    //main method used for every Java program
    public static void main(String[] args){
      
//to enable users to input info, construct an instance of Scanner
//this tells Scanner that your instance will take input from STDIN      
Scanner myScanner = new Scanner(System.in);

//print statement asking user to enter course number
System.out.println("Welcome!");
System.out.print("Please enter your course number: ");
 
//dummy variable
int testOne = 0;

//loop to ensure the correct type is entered      
while (testOne == 0){ //while the initialized variable equals zero, complete the following     
boolean confirmIntegerOne = myScanner.hasNextInt();//check the input to make sure it is of the type we want, in this case an integer
  
  if (confirmIntegerOne == true){ //if the input is an integer, complete the following 
   testOne += 1; //increase the value of the initialized variable in order to break out of the loop 
   int courseNumber = myScanner.nextInt(); //assign the input to the variable courseNumber
   //System.out.println("You entered " + courseNumber);
   //this print statement is meant to test that the input was correctly assigned to the specified variable 
  } //end if statement
  
  else if (confirmIntegerOne == false){ //if the input is not an integer, complete the following
   String junkOne = myScanner.next();//discard the input
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Please enter your course number: "); //ask user for new input
  
  }//end else if statement
}//end while loop

  
//print statement asking user to enter department      
System.out.print("Please enter the name of the department that offers this course: ");

//dummy variable      
int testTwo = 0;

//loop to ensure the correct type is entered      
while (testTwo == 0){ //while the initialized variable equals zero, complete the following 
//Note: we want the input to be string, so we need to test a little differently  
boolean confirmStringNotInt = myScanner.hasNextInt();//check the input to see if it is an integer
boolean confirmStringNotDouble = myScanner.hasNextDouble();//check the input to see if it is a double
 
 if (confirmStringNotInt == false & confirmStringNotDouble == false){//if the input is not an integer and is not a double, then it is likely a string
   testTwo += 1;//increase the value of the initialized variable in order to break out of the loop
   myScanner.nextLine(); //
   String departmentName = myScanner.nextLine();//accept the entire line of input as a string and assign it to the variable departmentName
   //System.out.println("You entered " + departmentName);  
   //this print statement is to test that the input was correctly assigned to the specified variable
 }//end if statement
  
 else if (confirmStringNotInt == true | confirmStringNotDouble == true){//if the input is either an integer or a double, then it is not a string
   String junkTwo = myScanner.next();//discard the input
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Please enter the name of the department that offers this course: ");//ask user for new input
 }//end else if statement 
}//end while loop
      
//print statement asking user to enter hours of class time per week     
System.out.print("Enter the number of hours this class meets per week: ");

//dummy variable      
int testThree = 0;
      
while (testThree == 0){//while the initialized variable equals zero, complete the following     
boolean confirmIntegerTwo = myScanner.hasNextInt();//check the input to make sure it is of the type we want, in this case an integer
  
 if (confirmIntegerTwo == true){//if the input is an integer
   testThree += 1;//increase the value of the initialized variable in order to break out of the loop
   int hoursMet = myScanner.nextInt();//assign the input to the variable hoursMet
   //System.out.println("You entered " + hoursMet);
   //this print statement is to test that the input was correctly assigned to the specified variable
 }//end if statement
  
 else if (confirmIntegerTwo == false){//if the input is not an integer 
   String junkThree = myScanner.next();//discard the input
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Enter the number of hours this class meets per week: ");//ask user for new input   
 }//end of else if statement
}//end of while loop      
            
//print statment asking user to enter the class start time      
System.out.print("Enter the time this class starts in the format xx.xx: ");

//dummy variable
int testFour = 0;
      
while (testFour == 0){//while the initialized variable equals zero, complete the following
boolean confirmDoubleOne = myScanner.hasNextDouble();//check the input to make sure it is of the type we want, in this case a double
  
  if (confirmDoubleOne == true){//if the input is a double
   testFour += 1;//increase the value of the initialozed variable in order to break out of the loop
   double startTime = myScanner.nextDouble();//assign the input to to teh variable startTime
   //System.out.println("You entered " + startTime);
   //this print statement is to test that the input was correctly assigned to the specified variable
  }//end of if statement
  
  else if (confirmDoubleOne == false){//if the input is not a double
   String junkFour = myScanner.next();//discard the input
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Enter the time this class starts in the format xx.xx: ");//ask user for new input
 }//end of else if statement 
}//end of while loop  
      
//all commands from here on are structured as above with the varying types
       
System.out.print("Enter the course instructor's name: ");

//dummy variable      
int testFive = 0;
      
while (testFive == 0){      
boolean confirmStringTwoNotInt = myScanner.hasNextInt();
boolean confirmStringTwoNotDouble = myScanner.hasNextDouble();
 
 if (confirmStringTwoNotInt == false & confirmStringTwoNotDouble == false){
   testFive += 1;
   myScanner.nextLine(); 
   String instructorName = myScanner.nextLine();
  // System.out.println("You entered " + instructorName);  
 }//end of if statement
  
 else if (confirmStringTwoNotInt == true | confirmStringTwoNotDouble == true){
   String junkFive = myScanner.next();
   System.out.println("Input Error. Try Again.");
   System.out.print("Please enter the course instructor's name: ");
 }//end of else if statement 
}//end of while loop  
      
      
      
System.out.print("Enter the number of students in this class: ");

//dummy variable      
int testSix = 0;
      
while (testSix == 0){      
boolean confirmIntegerThree = myScanner.hasNextInt();
 
 if (confirmIntegerThree == true){
   testSix += 1;
   int classSize = myScanner.nextInt();
  // System.out.println("You entered " + classSize);
 }//end of if statement
  
 else if (confirmIntegerThree == false){
   String junkSix = myScanner.next();
   System.out.println("Input Error. Try Again.");
  System.out.print("Enter the number of students in this class: "); 
 }//end of else if statement 
}//end of while loop  
      
      
    }//end of main method
}//end of class
//Ana Isabela Madrigal
//Homeowork 6: Argyle
//CSE002
//Tuesday, March 20, 2018
//I know there isn't much to show for it but I worked on this code for 12+ hours and couldnt even get close...hopefully I can at least get a point for submission


//imports scanner class
import java.util.Scanner;

public class Argyle{
    //main method required for every Java program
  public static void main(String[] args){
    
 //to enable users to input info, construct an instance of Scanner
//this tells Scanner that your instance will take input from STDIN     
Scanner myScanner = new Scanner(System.in); 

//print statement asking user to input an integer for their desired twist length     
System.out.print("Welcome! Enter the height of your argyle pattern: ");

 //dummy variable
int height = 0;

//loop to ensure the correct type is entered      
while (height == 0){ //while the initialized variable equals zero, complete the following     
boolean confirmHeightInt = myScanner.hasNextInt();//check the input to make sure it is of the type we want, in this case an integer
  
  if (confirmHeightInt == true){ //if the input is an integer, complete the following 
   //checkHeight += 1; //increase the value of the initialized variable in order to break out of the loop 
   height = myScanner.nextInt(); //assign the input to the variable courseNumber
  
  
  if (height < 0){
    height = 0;
    System.out.println("You entered a negative number. Please try again.");
    System.out.print("Please enter the height of your argyle pattern: ");
    
  }
  
      else{
    break;
  } 
  }//end if statement
  
  else if (confirmHeightInt == false){ //if the input is not an integer, complete the following
   String junk = myScanner.next();
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Please enter the height of your argyle pattern: "); //ask user for new input
  
  }//end else if statement
}//end while loop   
    
    

    
System.out.print("Enter the width of your argyle pattern: ");
    
    
 //dummy variable
int width = 0;

//loop to ensure the correct type is entered      
while (width == 0){ //while the initialized variable equals zero, complete the following     
boolean confirmWidthInt = myScanner.hasNextInt();//check the input to make sure it is of the type we want, in this case an integer
  
  if (confirmWidthInt == true){ //if the input is an integer, complete the following 
   //width = 0; //increase the value of the initialized variable in order to break out of the loop 
   width = myScanner.nextInt(); //assign the input to the variable courseNumber
  
  
  if (width < 0){
    width = 0;
    System.out.println("You entered a negative number. Please try again.");
    System.out.print("Please enter the width of your argyle pattern: ");
    
  }
    
  else{
    break;
  }  
  
  }//end if statement
  
  else if (confirmWidthInt == false){ //if the input is not an integer, complete the following
   String junk = myScanner.next();
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Please enter the width of your argyle pattern: "); //ask user for new input
  
  }//end else if statement
}//end while loop   
        
    
System.out.print("Enter the size of your argyle diamonds: ");

 //dummy variable
int diamondSize = 0;

//loop to ensure the correct type is entered      
while (diamondSize == 0){ //while the initialized variable equals zero, complete the following     
boolean confirmDiamondSizeInt = myScanner.hasNextInt();//check the input to make sure it is of the type we want, in this case an integer
  
  if (confirmDiamondSizeInt == true){ //if the input is an integer, complete the following 
   //checkDiamondSize += 1; //increase the value of the initialized variable in order to break out of the loop 
   diamondSize = myScanner.nextInt(); //assign the input to the variable courseNumber
  
  
  if (diamondSize < 0){
    diamondSize = 0;
    System.out.println("You entered a negative number. Please try again.");
    System.out.print("Please enter the size of your argyle diamonds: ");
    
  }
  
      else{
    break;
  } 
  }//end if statement
  
  else if (confirmDiamondSizeInt == false){ //if the input is not an integer, complete the following
   String junk = myScanner.next();
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Please enter the size of your argyle diamonds: "); //ask user for new input
  
  }//end else if statement
}//end while loop   
        
    
System.out.print("Enter the width of your argyle stripe: ");

 //dummy variable
int stripeWidth = 0;

//loop to ensure the correct type is entered      
while (stripeWidth == 0){ //while the initialized variable equals zero, complete the following     
boolean confirmStripeWidthInt = myScanner.hasNextInt();//check the input to make sure it is of the type we want, in this case an integer
  
  if (confirmStripeWidthInt == true){ //if the input is an integer, complete the following 
  // checkStripeWidth += 1; //increase the value of the initialized variable in order to break out of the loop 
    stripeWidth = myScanner.nextInt(); //assign the input to the variable courseNumber
  
  
  if (stripeWidth % 2 == 0){
    //String junk = myScanner.next();
    stripeWidth = 0;
    System.out.println("You entered an even number. Please try again.");
    System.out.print("Please enter the width of your argyle stripe: ");
    
  }
    
  else if (stripeWidth < 0 | stripeWidth >= diamondSize/2){
    String junk = myScanner.next();
 //   stripeWidth = 0;
    System.out.println("You entered too large a number. Please try again.");
    System.out.print("Please enter the width of your argyle stripe: ");
    
  }  
    
     // else{
    //break;
  //} 
  
  }//end if statement
  
  else if (confirmStripeWidthInt == false){ //if the input is not an integer, complete the following
   String junk = myScanner.next();
   System.out.println("Input Error. Try Again.");//print an error to the user
   System.out.print("Please enter the width of your argyle stripe: "); //ask user for new input
  
  }//end else if statement
}//end while loop   
        
    
System.out.print("Enter the character to fill your argyle stripe: ");
    
char stripeChar = myScanner.next().charAt(0);
    
System.out.print("Enter the character to fill your argyle diamond: ");
    
char diamondChar = myScanner.next().charAt(0);
    
System.out.print("Enter the character to fill the background of your pattern: ");
    
char background = myScanner.next().charAt(0);    
    

int a = diamondSize;
int b = diamondSize; 
int m = 1;
  
  for (int i = 0; i < height; i++){
   a--;     
    for (int j = 0; j < width; j++){
    
  int l = j % diamondSize;   
 
 if (m == 1){     
      
 switch (m){
   case 1: 
          if (l == diamondSize - a - 1){
      System.out.print(stripeChar);     
    continue;
    }
      
      
     else if (l < a + 1){
        System.out.print(background);
        
      }
     
        else {
        System.out.print(diamondChar);
        
      }
     
    if (l == diamondSize - 1){
       m += 1;
    }
     break;
   
   
   case 2:
     if (b == l + 1){
      System.out.print(stripeChar);
      continue;
    } 
      else if (l < diamondSize - b){
        System.out.print(diamondChar);
        
      }
     
       else {
        System.out.print(background);
        
      }
        
   b--;
    System.out.print("\n");
 m -= 1;
     break;
 
 }    
      
 // switch (n){
   // case 0:
      
      
      
  //}    
      
 }    

      
  //else if (a == j + 1){
    //  System.out.print(" ");
      
    //}  
     //else if (j ){
       // System.out.print(diamondChar);
        
      //}
      
   
      
      
      
    //}
    

   
  }
  }
  }
}
    
//for (int l = 0; l < diamondSize; l++){
      
  /*  for (int k = 0; k < width; k++){
     int l = k % diamondSize;
       
      
     //else if (j ){
       // System.out.print(diamondChar);
        
      //}
      
    
      
      
      
   
  }
  
    
 /*   
int c = diamondSize + 1;
int d = diamondSize;
    
    
for (int i = 0; i < height; i++){
   c--;     
    for (int j = 0; j < width; j++){
     int l = j % diamondSize;
      
      if (c == j + 1){
      System.out.print(stripeChar);
      continue;
    } 
      
      if (j < diamondSize - c){
        System.out.print(background);
        
      }

  //System.out.print("*"); 
    
  
     //else if (j ){
       // System.out.print(diamondChar);
        
      //}
      
      else {
        System.out.print(diamondChar);
        
      }
      
      
      
    }
    

//int d = diamondSize;
    
   // for (int i = 0; i < diamondSize; i++){
   d--;     
    for (int k = 0; k < width; k++){
      int l = k % diamondSize;
       if (k == diamondSize - d - 1){
      System.out.print(stripeChar);  
    continue;
    }
      
      if (k < d + 1){
        System.out.print(diamondChar);
        
      }
      
     //else if (j ){
       // System.out.print(diamondChar);
        
      //}
      
      else {
        System.out.print(background);
        
      }
      
      
      
    }
    

    System.out.print("\n");
  }
    
  
  }
}
  */  
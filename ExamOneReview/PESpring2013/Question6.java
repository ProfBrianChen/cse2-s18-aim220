//prompts and accepts three integers from user
//computes average of integers
//prints decimal average
//if average is positive print "average is positive"
//if average is zero print "average is zero"
//if average is negative print "average is negative"


import java.util.Scanner;

public class Question6 {
  
  public static void main(String[] args){
 
 Scanner scan = new Scanner ( System.in );
 System.out.println("Input 3 integers");
 
  
 
 int firstNumber = scan.nextInt();
 int secondNumber = scan.nextInt();
 int thirdNumber = scan.nextInt();
    
 double average = ((double) firstNumber + (double) secondNumber + (double) thirdNumber)/3;

 System.out.println("The decimal average is: " + average );
    
 if (average > 0){
   System.out.println("The average is positive");
 }   
 
 else if (average < 0){
   System.out.println("The average is negative");
 }   
   
 else if (average == 0){
   System.out.println("The average is zero");
 }   
  }
}
//What is the final value of x?



public class Question3 {
  
  public static void main(String[] args){
    
///snippet 1
//int j = 10, k = 1;
//double z = 3.14;
//String t = "sink";
//String x = j * k + z + t;
 
//System.out.println("The final value of x is: " + x);
    

//snippet 2
//double y = 250.0, x = 124.2, z = 40.4;

//y = z;
//z = x;
//x = y;
//z = y/2;
//x = (int) z;

//System.out.println("The final value of x is: " + x);    
    
    
//snippet 3
//double z = 3.0, y = 2.4; 
//int x = 2;

//z = x + y;
//x = z;
//z -= x + y;
//x += z;
//x += y;
 
//snippet 4
boolean p = true, q = false;
boolean x;
x = (!p || q) && (!q && p);
System.out.println("x is " + x);    
    
    
  }
}
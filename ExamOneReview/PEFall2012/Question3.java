//What is the output of the following code?

public class Question3 {
  
  public static void main(String[] args){
    
    int i;
    i = 3;
    switch(i){
      case 1:
      case 2:
      case 3:
        System.out.println("hello!");
      case 4: 
        System.out.println("goodbye!");
        break;
      case 5:
      case 3:
        System.out.println("testing!");
        break;
      default: 
        System.out.println("default!");
        break;    
    }
  }
}
//Write code that sorts 3 integers from the user
//prints them in numerical order from low to high
//assume program is in the file Question12.java


import java.util.Scanner;

public class GoodQuestion12 {
  
  public static void main(String[] args){
    
  Scanner scan = new Scanner( System.in );
  
  System.out.println ("Enter your first number: ");
  int x = scan.nextInt ();
    
  System.out.println ("Enter your second number: ");  
  int y = scan.nextInt ();
    
  System.out.println ("Enter your third number: ");
  int z = scan.nextInt ();
    
    
 if ((x > y) && (y > z)){
   System.out.println("The numbers from lowest to highest are: " + z + y + x); 
 }
 
 else if ((x > z) && (z > y)){
   System.out.println("The numbers from lowest to highest are: " + y + z + x);
 }  
    
 else if ((z > y) && (y > x)){
   System.out.println("The numbers from lowest to highest are: " + x + y + z);
 } 
    
 else if ((z > x) && (x > y)){
   System.out.println("The numbers from lowest to highest are: " + y + x + z);
 } 
    
 else if ((y > z) && (z > x)){
   System.out.println("The numbers from lowest to highest are: " + x + z + y); 
 } 
    
 else if ((y > x) && (x > z)){
   System.out.println("The numbers from lowest to highest are: " + z + x + y);
 }
    
  }
}
//Write Java code that uses the Math.random() method to generate a random integer from 1 to 100.

public class Question9 {
  
  public static void main(String[] args){
    
int randomNumber = (int)(Math.random()* 100) + 1;
    
    System.out.println("Your random number is: " + randomNumber);
    
  }
}
//In the following code snippets, what is the final value of x?

public class Question4 {
  
  public static void main(String[] args){

//a)    
//int x = 35;
//int y = 25;
//y = x;
//x = y;

//b)    
//double y = 124.5, x = 312.5, z = 37.5;
//y = z;
//z = x; 
//x = y;
//z = y;
//x = z;
   
//c)        
//double w, y = 3.2, x = 3.2, z = 6.4;
//w = z - y - x;
//x = y - 3 * w + z;
//z = x + y;
//x = w + z;
   
System.out.println("The final value of x is: " + x);     
    
  }
}
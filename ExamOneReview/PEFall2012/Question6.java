//Convert the following if statement into a switch statement


public class Question6 {
  
  public static void main(String[] args){
    
// Find interest rate based on year
//int numOfYears = 7;
//int numOfYears = 15;
//int numOfYears = 30;
//int numOfYears = 5;
double annualInterestRate = 0;
    
if (numOfYears == 7) {
annualInterestRate = 3.25;
  System.out.println("The annual interest rate is " + annualInterestRate);
} 
    
else if (numOfYears == 15) {
annualInterestRate = 3.50;
  System.out.println("The annual interest rate is " + annualInterestRate);
} 
    
else if (numOfYears == 30) {
annualInterestRate = 4.0;
  System.out.println("The annual interest rate is " + annualInterestRate);
} 
    
else {
System.out.println("Wrong number of years");
}

switch (numOfYears){
  case 7:
    annualInterestRate = 3.25;
    System.out.println("The annual interest rate is " + annualInterestRate);
    break;
  case 15:
    annualInterestRate = 3.50;
    System.out.println("The annual interest rate is " + annualInterestRate);
    break;
  case 30:
    annualInterestRate = 4.0;
    System.out.println("The annual interest rate is " + annualInterestRate);
    break;
  default:
    System.out.println("Wrong number of years");
    break;
    
}   
    
    
  }
}
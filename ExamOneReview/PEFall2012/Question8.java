//Below, write a single if-else statement verifying
//the integer v is in the range 1 to 10 (inclusive)
//prints out “yes” if so or “no”, otherwise.



public class Question8 {
  
  public static void main(String[] args){
 
int v;
//v = 1;    
//v = 5;
v = 10;
//v = 0;
//v = 11;
    
if (v > 0 && v < 11){
  System.out.println("yes");
}
    
else {
  System.out.println("no");
}    
    
  }
}
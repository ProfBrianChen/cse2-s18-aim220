//Add parentheses to the complex expressions 
//according to operator precedence and associativity


public class Question11 {
  
  public static void main(String[] args){
 
double x = 32.4;
double y = 357.2;
    
boolean z = (int) x + 3 * (int) y * 32.2 < 73;
    System.out.println(z);
    System.out.println("The value is " + ((int) x + 3 * (int) y * 32.2));

boolean a = (((int) x) + (3 * ((int) y) * 32.2)) < 73;
    System.out.println(z);
    System.out.println("The value is " + (((int) x) + ((3 * ((int) y)) * 32.2)));
 
String p = "test ";
String q = "exam ";
String r = "cse2 ";
    
String t = p + 3 * ((int)25.5 * (int)39.3) / 1.2;
    System.out.println(t);

String s = ((p + ((3 * (((int)25.5) * ((int)39.3)) / 1.2))) );    
  System.out.println(s);
  
  
  }
}
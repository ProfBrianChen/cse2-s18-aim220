//Evaluate the Java expressions in the boxes below
//assume the following variable declarations and values

public class Question5 {
  
  public static void main(String[] args){
    
 int i = 0, j = 15, k = 7;
 double x = -40.0, y = 0.0, z = 3.14;
 String s = "fall", t = "break";
 boolean p = false, q = true;
 char c = 'D';

//a)    
if (p && !q){ //|| (q && !p)){
System.out.println("true");
}
else {    
System.out.println("false");
  }
    
//System.out.println(p && !q);    
  
//b)    
if (i == y){
  System.out.println("true");
}    
else {    
System.out.println("false");
  }    

//c)    
if (j/k>z-1){
  System.out.println("true");
}    
else {    
System.out.println("false");
  }    

//d)   
char answerd = (char)(c + k - 5);
  System.out.println("The character is " + answerd);

//e)    
if (s + t == "fallbreak"){
  System.out.println("true");
}    
else {    
System.out.println("false");
  }    
 
//f)    
//if (t < s){
//  System.out.println("true");
//}    
//else {    
//System.out.println("false");
//  }    
 
//g)    
System.out.println("fall" + "break" + 2 + 1);
    
//h)
System.out.println(1 + 2 + "fall" + "break");
    
    
//i)    
//char answeri = (char) s;
//    System.out.println("The character is " + answeri);
 
  
  }   
}
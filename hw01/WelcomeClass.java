//cse2 Welcome Class
public class WelcomeClass {
  
  public static void main(String[] args){
    
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-A--I--M--2--2--0->");    
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");    
    System.out.println(" \n My name is A. Bela Madrigal. The 'A' stands for Ana. Except I don't like Ana. I just like 'A' and that's all.");
  }
  
}
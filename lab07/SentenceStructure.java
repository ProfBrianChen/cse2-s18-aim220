//Ana Isabela Madrigal
//Friday, March 23, 2018
//CSE 002
//This code is meant to take a user input for the size of a grid of stars with a hidden x

//imports scanner class
import java.util.Scanner;

import java.util.Random;

public class SentenceStructure{
    
  
  public static String adjectives(){
    
  String a = " ";  
  Random randomGenerator = new Random();  
  int randomNumber = randomGenerator.nextInt(10);
    
    
    
    switch (randomNumber){
      case 0: 
      a = "lazy";
      break;
      
      case 1:
      a = "stubborn";
      break;
      
      case 2:
      a = "insufferable";
      break;
      
      case 3:
      a = "obnoxious";
      break;
      
      case 4:
      a = "dim-witted";
      break;
      
      case 5:
      a = "lovely";
      break;
      
      case 6:
      a = "elegant";
      break;
      
      case 7:
      a = "regal";
      break;
      
      case 8:
      a = "brilliant";
      break;
      
      case 9:
      a = "remarkable";
      break;
    
    }
   return a;
    
  }
  
  public static String subject(){
  String b = " ";  
  Random randomGenerator = new Random();  
  int randomNumber = randomGenerator.nextInt(10);
    
    
    
    switch (randomNumber){
      case 0: 
      b = "queen";
      break;
      
      case 1:
      b = "princess";
      break;
      
      case 2:
      b = "king";
      break;
      
      case 3:
      b = "prince";
      break;
      
      case 4:
      b = "dutchess";
      break;
      
      case 5:
      b = "duke";
      break;
      
      case 6:
      b = "regent";
      break;
      
      case 7:
      b = "advisor";
      break;
      
      case 8:
      b = "general";
      break;
      
      case 9:
      b = "lady-in-waiting";
      break;
    
    }
   
    return b;
  }
  
  public static String verb(){
  String c = " ";  
  Random randomGenerator = new Random();  
  int randomNumber = randomGenerator.nextInt(10);
    
    
    
    switch (randomNumber){
      case 0: 
      c = "broke";
      break;
      
      case 1:
      c = "shook";
      break;
      
      case 2:
      c = "rattled";
      break;
      
      case 3:
      c = "lifted";
      break;
      
      case 4:
      c = "tore";
      break;
      
      case 5:
      c = "undermined";
      break;
      
      case 6:
      c = "encouraged";
      break;
      
      case 7:
      c = "overthrew";
      break;
      
      case 8:
      c = "annoyed";
      break;
      
      case 9:
      c = "interrupted";
      break;
    
    }
    return c;
  }
  
  public static String object(){
  String d = " ";  
  Random randomGenerator = new Random();  
  int randomNumber = randomGenerator.nextInt(10);
    
    
    
    switch (randomNumber){
      case 0: 
      d = "army";
      break;
      
      case 1:
      d = "spirits";
      break;
      
      case 2:
      d = "crowd";
      break;
      
      case 3:
      d = "assembly";
      break;
      
      case 4:
      d = "party";
      break;
      
      case 5:
      d = "royal family";
      break;
      
      case 6:
      d = "bride";
      break;
      
      case 7:
      d = "groom";
      break;
      
      case 8:
      d = "baby";
      break;
      
      case 9:
      d = "cries";
      break;
    
    }
  return d;
  }
  
  public static void secondSubject(String subjectCarry){
   
    String subTwo = "";
    
  Random randomGenerator = new Random();  
  int randomNumber = randomGenerator.nextInt(2);  
   
   // for ()
    switch (randomNumber){
        case 0: 
         subTwo = "The " + subjectCarry;
         break;
          
        case 1:
         subTwo = "They";
         break;
      }
    
    //String composedSentence = 
      bodySentences(subTwo); 
    
    
    
  }
  
  public static void bodySentences(String firstWord){
   Random randomGenerator = new Random();  
  int randomNumber = randomGenerator.nextInt(3);  
    for (int i = 0; i <= randomNumber; i++){
    
    String adjNext = adjectives();
   // String subNext = subject();
    String verbNext = verb();
    String objNext = object();
    String secObj = object();
    
    System.out.println("");  
    System.out.println(firstWord + " told the " + objNext + " to " + verbNext + " the " + adjNext + " " + secObj + ".");
    System.out.println("");  
  }
  }
  
  public static void conclusion(String finalSubject){
    
    
    
    String verbNext = verb();
    String objNext = object();
   
    
    System.out.println("That " + finalSubject + " " + verbNext + " the " + objNext + "!");
    
  }
  
  //main method required for every Java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    
    String adjOne = adjectives();
    String subOne = subject();
    String verbOne = verb();
    String objOne = object();
    
    System.out.println("");
    System.out.println("The " + adjOne + " " + subOne + " " + verbOne + " the " + objOne + ".");
    System.out.println("");
    
    
    int dummy = 0;
    String subTwo = "";
    
    do{
    
    System.out.print("Would you like more sentences? Enter yes or no: ");
    
    String answer = myScanner.next();
    
    if (answer.equals("yes")){
      
      //String secondSentence = 
        secondSubject(subOne);
      //String conclusionSentence = 
        conclusion(subOne);
      
      dummy = 1;
    }
    
    else if (answer.equals("no")){
      break;
    }  
    
    else{
      System.out.println("Input Error. Try Again.");
    }
    }while(dummy == 0);
  
  
  
  
  
  
  
  
  
  
  
  }
  
}
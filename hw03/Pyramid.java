//Ana Isabela Madrigal
//Homework 3: Pyramid
//Tuesday, February 13, 2018
//CSE002
//This program is meant to accept user input of the dimensions of a pyramid in order to calculate its volume


//imports Scanner class
    import java.util.Scanner;

    public class Pyramid{
     //main method required for every Java program 
      public static void main(String[] args){
        
        
//to enable users to input info, construct an instance of Scanner
//this tells Scanner that your instance will take input from STDIN
Scanner myScanner= new Scanner ( System.in);
 //prompt user for length of base
System.out.print("Enter the base length of the pyramid: ");
//accept user input 
double baseLength= myScanner.nextDouble();      
//prompt user for width of base 
System.out.print("Enter the base width of the pyramid: ");    
//accept user input
double baseWidth= myScanner.nextDouble();
//prompt user for height of pyramid
System.out.print("Enter the height of the pyramid: ");        
//accept user input
double height= myScanner.nextDouble();
//prompt user for number of people splitting the check
      
      
//print output
double pyramidVolume;//declare variable
pyramidVolume= (baseLength*baseWidth*height)/3; //volume of the pyramid with specified dimensions
        
System.out.println("The volume of the pyramid is " + pyramidVolume + " cubic units");
        
      }//end of main method
    }//end of class
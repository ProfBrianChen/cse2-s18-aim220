//Ana Isabela Madrigal
//Homework 3: Convert
//Tuesday, February 13, 2018
//CSE002
//This program is meant to receive input from the user about the volume of rainfall over a specified number of acres to convert the rainfall into cubic miles


//imports the scanner class
  import java.util.Scanner;

  public class Convert{
   //main method required for every java program 
    public static void main(String[] args){
      
      
//to enable users to input info, construct an instance of Scanner
//this tells Scanner that your instance will take input from STDIN
Scanner myScanner= new Scanner ( System.in);   
      
      
//prompt user for affected area in acres
System.out.print("Enter the acres of land affected by rainfall: ");
//accept user input
double acresAffected= myScanner.nextDouble();
//prompt user for average rainfall in inches
System.out.print("Enter the average rainfall in inches: ");
//accept user input
double rainInches= myScanner.nextDouble();
//declare variable 
double rainMiles, squareMiles, totalRainfall;      
//convert inches to cubic miles
rainMiles= rainInches*(1.58* Math.pow (10,-5));
//convert acres to square miles
squareMiles= acresAffected*(1.56* Math.pow (10,-3));
//multiply area affected by amount of rainfall
totalRainfall= rainMiles*squareMiles;
//print output
System.out.println("The average rainfall in the affected area is " + totalRainfall + " cubic miles.");
      
      
    }//end of main method
  }//end of class
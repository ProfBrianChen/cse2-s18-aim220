//Ana Isabela Madrigal
//Friday, March 9, 2018
//CSE 002
//This code is meant to take a user input for the size of a grid of stars with a hidden x

//imports scanner class
import java.util.Scanner;

public class encrypted_x{
    //main method required for every Java program
  public static void main(String[] args){
    
 //to enable users to input info, construct an instance of Scanner
//this tells Scanner that your instance will take input from STDIN     
Scanner myScanner = new Scanner(System.in); 

//print statement asking user to input an integer for their desired twist length     
System.out.print("Welcome! Enter any integer between 0 and 100 to determine the size of your grid: ");    
    
int input = 0;
    
while (input == 0){
  boolean integer = myScanner.hasNextInt();
  
if (integer == true){
  input = myScanner.nextInt();
  
 if (input < 0){
  System.out.print("Input error; Enter any integer for your desired twist length: ");
  input = 0; 
 }
  
 else{
   break;
 } 
}
   
  else if (integer == false){
    String junk = myScanner.next();
    System.out.print("Input error; Enter any integer between 0 and 100 to determine the size of your grid: ");
    
  }
}
    
int k = input;
    
for (int i = 0; i < input; i++){
 
  for(int j = 0; j < input; j++){
  //System.out.print("*"); 
    
    if (j == input - k){
      System.out.print(" ");  
    }
    
    
  else if (k == j + 1){
      System.out.print(" ");
      
    }  
    
    else {
      System.out.print("*");
      
    }
   // j++;
      
    //  else if ()
      
    
    
  }
  k--;
  System.out.print("\n");

}    
    
  
  
  
  
  
  

  
  
  
  
  
  }
}
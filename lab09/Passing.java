//Ana Isabela Madrigal
//Friday, April 13, 2018
//CSE 002
//This code is meant to 


//In your main method, declare a literal integer array of length at least 8 and make two copies of it using the copy() method, called array0, array1, array2.

import java.util.Random;

public class Passing{
  

  public static void main(String[] args){
    
    
    int arrayValue = 0;
    int [] array0 = new int[10];
    System.out.print("The original array is: ");
    for(int i = 0; i < 10; i++){
    
    arrayValue = (int) (Math.random() * 10);
    
    array0[i] = arrayValue;
    
    System.out.print(array0[i]);
    
  } 
  
 System.out.println("");
    
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
    inverter(array0);
    print(array0);
    
    inverter2(array1);
    print(array1);
    
    int [] array3 = inverter2(array2);
    print(array2);
    
}//end main method
  
  
  public static int[] copy (int[] copyArray){
  
    int [] newArray = new int[copyArray.length];
  
  for (int i=0; i<copyArray.length; i++){
    newArray[i] = copyArray[i];
    
  }//end for loop
  
  return newArray;
    
  }//end method copy
  
  
  
  public static void inverter(int[] firstFlip){
    
    for(int i=0; i< firstFlip.length/2; i++){
    
    int temp = firstFlip[i];
    firstFlip[i] = firstFlip[firstFlip.length - i - 1]; 
    firstFlip[firstFlip.length - i - 1] = temp;
      
    }//end for loop
    
    
  }//end method inverter
  
  public static int [] inverter2(int[] secondFlip){
    
    int [] copyArray = copy(secondFlip);
    
    for(int i=0; i< copyArray.length/2; i++){
    
    int temp = copyArray[i];
    copyArray[i] = copyArray[copyArray.length - i - 1]; 
    copyArray[copyArray.length - i - 1] = temp;
      
    }//end for loop
      return copyArray;
    
    
  }//end method inverter2
  
  public static void print(int[] printArray){
    
    for (int i=0; i< printArray.length; i++){
      System.out.print(printArray[i]);
     
      
    }//end for loop
    
    System.out.println("");
  }//end method print
  
  
  
    
}//end class
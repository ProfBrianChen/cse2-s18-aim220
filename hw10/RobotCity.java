//Ana Isabela Madrigal
//Homework 10: Robot City
//CSE002
//Tuesday, April 24, 2018
//This code is meant to invade a city with robots by using methods and multidimensional arrays





public class RobotCity{
  
  public static void main(String[] args){
    
    //call method to determine the length of the block
    int cityLength = cityLength();
    //call method to determine the width of the block
    int cityWidth = cityWidth();
    //determine the area of the block, which is the max number of invaders possible
    int blockSize = cityLength * cityWidth;
    
    //determine a random number of invaders
    int randomInvaders = (int) (Math.random() * blockSize);
    
    //call method to build the city
    int [] [] cityArray = buildCity(cityLength, cityWidth);
    //call method to print the block
    display(cityArray);
    //space
    System.out.println(" ");
    
    //call method to implement robots
    invade(cityArray, randomInvaders);
    
    //print update five times
    for (int i=0; i<5; i++){
    update(cityArray);
    System.out.println(" ");
    }
  }//end main method 
  
  //method to determing length of city
  public static int cityLength(){
    int cityLength = (int) (Math.random () * 5) + 10;
    return cityLength;
  }//end method cityLength
  
  //method to determining width of city
  public static int cityWidth(){
    int cityWidth = (int) (Math.random() * 5) +10;
    return cityWidth;
  }//end method cityWidth
  
  //method to build city block
  public static int [][] buildCity (int cityLength, int cityWidth){
    
    
    int [][] blocks = new int [cityLength][cityWidth];
   
    
    //for each coordiante starting from bottom to top, assign a value between 100 and 999
    for (int i=cityLength - 1; i >= 0; i--){
      for (int j=0; j< cityWidth; j++){
        
        blocks [i][j] = (int) (Math.random () * 899) + 100;
        
        
        
      }//end nested loop
    }//end for loop
    
    return blocks;
   
  }//end method buildCity
  
  
  
  

  
  //method to display the city block
  public static void display (int [][] cityArray){
    
    //starting from bottom to top print values
    for (int i = cityArray.length - 1; i >= 0; i--){
      for (int j=0; j< cityArray[0].length; j++){
        
        //if value is positive, add an extra space to account for the formatting of the negative signs
        if (cityArray[i][j] > 0){
        System.out.print( " " + cityArray[i][j] + "  ");
        }
        
        //if value is negative, print normally
        else if (cityArray[i][j] < 0){
          System.out.print(cityArray[i][j] + "  ");
        }
      }//end nested loop
      
      System.out.println("");
        
        
            
      }//end nested loop
      
      
  }//end method display
  
  
  
  //method to implement invaders
  public static void invade (int [][] cityArray, int k ){

    //pick a random coordinate to assign an invader to
    for (int i = 0; i < k; i++){
    int horizontal = (int) (Math.random () * cityArray.length);
    int vertical = (int) (Math.random () * cityArray[0].length);
    
      //for that coordinate, double the value and subtract it from itself to make it negative
      cityArray[horizontal][vertical] -= 2 * cityArray[horizontal][vertical];
      
    }
    
    //print
     for (int h = cityArray.length - 1; h >= 0; h--){   
      for (int j=0; j< cityArray[0].length; j++){
    

      
        if (cityArray[h][j] > 0){
        System.out.print( " " + cityArray[h][j] + "  ");
        }
        
        else if (cityArray[h][j] < 0){
          System.out.print(cityArray[h][j] + "  ");
        }
      }//end nested loop
      
      System.out.println("");
      
    
    }//end for loop
 
    System.out.println("");
  }//end method invade
  
  
  //method to move invaders eastward
  public static void update (int [][] cityArray){
    
    //for j moving from right to left
     for (int i=0; i < cityArray.length; i++){
      for (int j=cityArray[0].length - 1; j >= 0; j--){
        
        //if the value in the final position is negative, make it positive
        //I realized all I had to do was multiply the value by negative 1...progress
        if (j == (cityArray[0].length -1) & cityArray[i][j] < 0){
          cityArray[i][j] = (-1) * cityArray[i][j];
        continue;
        }
        
        //if the value is negative, assign the value next to it as a negative number and reassign the value itself to be positive
        else if (cityArray[i][j] < 0){
          cityArray[i][j+1] = (-1) * cityArray[i][j+1];
          cityArray[i][j] = (-1) * cityArray[i][j];
          
          
        }//end if statement
        
      
      }
     }
    
        //print
        display(cityArray);    
    
    
  }//end method update
  
  
  
}//end class
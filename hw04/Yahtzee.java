//Ana Isabela Madrigal
//Homework 4: Yahtzee!
//CSE002
//Tuesday, February 20, 2018
//This code is meant to model the dice game "Yahtzee" by rolling five dice and computing an appropriate score


//imports the Scanner class
import java.util.Scanner;

public class Yahtzee{
  //main method used for every Java program
  public static void main(String[]args){
    

//to enable users to input info, construct an instance of Scanner
//this tells Scanner that your instance will take input from STDIN    
Scanner myScanner = new Scanner (System.in);

//print statement asking for user to roll or select numbers
System.out.print("Welcome to Yahtzee! To roll the dice, type 1. To pick your numbers, type 2: ");   

//accept user input
int rollOrChoose = myScanner.nextInt();
    
int dieOne = 0;
int dieTwo = 0;
int dieThree = 0;
int dieFour = 0;
int dieFive = 0;
//if the user chooses to roll, use Math method to generate 5 random numbers to signify rolling 5 dice
//note that the numbers should be between 1 and 6 inclusively
//print the randomly generated numbers for the user
if (rollOrChoose == 1){
  dieOne = (int)(Math.random()* 5)+1;
  dieTwo = (int)(Math.random()* 5)+1;
  dieThree = (int)(Math.random()* 5)+1;
  dieFour = (int)(Math.random()* 5)+1;
  dieFive = (int)(Math.random()* 5)+1;
  
 
  
  int upperSectionTotal = dieOne + dieTwo + dieThree + dieFour + dieFive;
    
  System.out.println("Your numbers are: " + dieOne + " " + dieTwo + " " + dieThree + " " + dieFour + " " + dieFive);
  System.out.println("The Upper Section Initial Total is: " + upperSectionTotal);
}  //end of if statement
    
   
//if the user chooses to select their own numbers, follow these steps    
else if (rollOrChoose == 2){
  
  //print statement asking for 5 numbers within the range of a die
  System.out.print("Enter 5 numbers between 1 and 6: ");
  
  //accept user input
  int chosenNumbers = myScanner.nextInt();
  
  //if the user enters more than 5 numbers, they will receive an error
  if (chosenNumbers < 10000 || chosenNumbers > 99999){
    System.out.println("Digit Error.");//print error statement
    System.out.println("Try Again.");
    System.exit(1);
  }//end of if statement
  
  
  //initialize variables for each number entered by the user
  //use modulus to account for each digit of the five digit number
  dieOne = (chosenNumbers/10000) % 10;
  dieTwo = (chosenNumbers/1000) % 10;
  dieThree = (chosenNumbers/100) % 10;
  dieFour = (chosenNumbers/10) % 10;
  dieFive = (chosenNumbers) % 10;

  //initialize a variable to be used as a holder
  //this will prevent our next error statement from printing out more than once for several errors
  int errorValue = 0;
  
  //use a switch statement for each entered number to make sure the numbers are within the specified range
  //if the number is less than one or greater than six, then the holder variable will equal 1
  switch (dieOne){
    case 0:
    case 7:
    case 8:
    case 9:
  errorValue = 1;  
  }//end of switch statement
  
  
  switch (dieTwo){
    case 0:
    case 7:
    case 8:
    case 9:
  errorValue = 1;   
  }//end of switch statement
  
  switch (dieThree){
    case 0:
    case 7:
    case 8:
    case 9:
 errorValue = 1;   
  }//end of switch statement
  
  
  switch (dieFour){
    case 0:
    case 7:
    case 8:
    case 9:
 errorValue = 1;
  }//end of switch statement
  
  
  switch (dieFive){
    case 0:
    case 7:
    case 8:
    case 9:
  errorValue = 1;  
  }//end of switch statement
  
  //if the holder variable equals 1, then the user will receive an error
  if (errorValue == 1){
    System.out.println("Range Error.");//print error statement
    System.out.println("Try Again.");
    System.exit(1);
  }//end of if statement
  
}//end of else if statement 

    
int upperSectionTotal = dieOne + dieTwo + dieThree + dieFour + dieFive;   
System.out.println("The Upper Section Initial Total is: " + upperSectionTotal);
   
    
int upperSectionTotalWithBonus = 0;    
if (upperSectionTotal > 63){
  upperSectionTotalWithBonus = upperSectionTotal + 35;
  System.out.println("You got a Bonus!");
  System.out.println("Your Upper Section Initial Total with Bonus is: " + upperSectionTotalWithBonus);
}    

else {
  System.out.println("No Bonus");
}
    
 int countOne = 0;
 int countTwo = 0;
 int countThree = 0;
 int countFour = 0;
 int countFive = 0;
 int countSix = 0;
    
    
 int threeOfAKind = 0;
 int fourOfAKind = 0;
 int yahtzee = 0;
 int smallStraight = 0;
 int largeStraight = 0;
 int chance = 0;
 int fullHouse = 0;
 
    
    
switch (dieOne){
  case 1:
    countOne = countOne + 1;
    break;
    
  case 2:
    countTwo = countTwo + 1;
    break;
    
  case 3:
    countThree = countThree + 1;
    break;
    
  case 4:
    countFour = countFour + 1;
    break;
    
  case 5:
    countFive = countFive + 1;
    break;
    
  case 6:
    countSix = countSix + 1;
    break;
    
}//end switch
 
switch (dieTwo){
  case 1:
    countOne = countOne + 1;
    break;
    
  case 2:
    countTwo = countTwo + 1;
    break;
    
  case 3:
    countThree = countThree + 1;
    break;
    
  case 4:
    countFour = countFour + 1;
    break;
    
  case 5:
    countFive = countFive + 1;
    break;
    
  case 6:
    countSix = countSix + 1;
    break;
    
}//end switch    
    
switch (dieThree){
  case 1:
    countOne = countOne + 1;
    break;
    
  case 2:
    countTwo = countTwo + 1;
    break;
    
  case 3:
    countThree = countThree + 1;
    break;
    
  case 4:
    countFour = countFour + 1;
    break;
    
  case 5:
    countFive = countFive + 1;
    break;
    
  case 6:
    countSix = countSix + 1;
    break;
    
}//end switch 

switch (dieFour){
  case 1:
    countOne = countOne + 1;
    break;
    
  case 2:
    countTwo = countTwo + 1;
    break;
    
  case 3:
    countThree = countThree + 1;
    break;
    
  case 4:
    countFour = countFour + 1;
    break;
    
  case 5:
    countFive = countFive + 1;
    break;
    
  case 6:
    countSix = countSix + 1;
    break;
    
}//end switch    
    
switch (dieFive){
  case 1:
    countOne = countOne + 1;
    break;
    
  case 2:
    countTwo = countTwo + 1;
    break;
    
  case 3:
    countThree = countThree + 1;
    break;
    
  case 4:
    countFour = countFour + 1;
    break;
    
  case 5:
    countFive = countFive + 1;
    break;
    
  case 6:
    countSix = countSix + 1;
    break;
    
}//end switch    

if ((countOne == 3 || countTwo == 3 || countThree == 3 || countFour == 3 || countFive == 3 || countSix == 3) && (countOne == 1 || countTwo == 1 || countThree == 1 || countFour == 1 || countFive== 1 || countSix == 1)){
  
  switch (dieOne){
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      threeOfAKind = (3 * dieOne);
  }
  
   switch (dieTwo){
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      threeOfAKind = (3 * dieTwo);
  }
  
   switch (dieThree){
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      threeOfAKind = (3 * dieThree);
  }
  
   switch (dieFour){
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      threeOfAKind = (3 * dieFour);
  }
  
   switch (dieFive){
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      threeOfAKind = (3 * dieFive);
  }
  
  System.out.println("You rolled three of a kind!");
  System.out.println("Your score is: " + threeOfAKind);
  
  
} 
    
if (countOne == 4 || countTwo == 4 || countThree == 4 || countFour == 4 || countFive == 4 || countSix == 4){
  
  System.out.println("You rolled four of a kind!");
  System.out.println("Your score is: ");
}    

    
if ((countOne == 3 || countTwo == 3 || countThree == 3 || countFour == 3 || countFive == 3 || countSix == 3 ) && (countOne == 2 || countTwo == 2 || countThree == 2 || countFour == 2 || countFive == 2 || countSix == 2)){
  fullHouse = 25;
  System.out.println("You rolled a full house!");
  System.out.println("Your score is: " + fullHouse);
  
} 
    
if (countOne == 5 || countTwo == 5 || countThree == 5 || countFour == 5 || countFive == 5 || countSix == 5){
  yahtzee = 50;
  System.out.println("Yahtzee!");
  System.out.println("Your score is: " + yahtzee);
}

if ((countOne >= 1 && countTwo >= 1 && countThree >= 1 && countFour >= 1 && countFive >= 0 && countSix >= 0) || (countOne >= 0 && countTwo >= 1 && countThree >= 1 && countFour >= 1 && countFive >= 1 && countSix >= 0) || (countOne >= 0 && countTwo >= 0 && countThree >= 1 && countFour >= 1 && countFive >= 1 && countSix >= 1)){
  smallStraight = 30;
  System.out.println("You rolled a small straight!");
  System.out.println("Your score is: " + smallStraight);  
}  
  
if ((countOne >= 0 && countTwo >= 1 && countThree >= 1 && countFour >= 1 && countFive >= 1 && countSix >= 1) || (countOne >= 1 && countTwo >= 1 && countThree >= 1 && countFour >= 1 && countFive >= 1 && countSix >= 0)){
  largeStraight = 40;
  System.out.println("You rolled a large straight!");
  System.out.println("Your score is: " + largeStraight);
  
}    

chance = dieOne + dieTwo + dieThree + dieFour + dieFive;
System.out.println("The Chance is: " + chance);
    
int lowerSectionTotal = threeOfAKind + fourOfAKind + yahtzee + fullHouse + smallStraight + largeStraight + chance;
    
System.out.println("The Lower Section Total is: " + lowerSectionTotal);

int grandTotal = lowerSectionTotal + upperSectionTotal + upperSectionTotalWithBonus;

System.out.println("The Grand Total is: " + grandTotal);
    
  }//end of main method
}//end of class

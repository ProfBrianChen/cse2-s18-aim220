//Ana Isabela Madrigal
//Friday February 16, 2018
//CSE002
//This program is meant to randomly select a card from a deck of 52, displaying the number and suit

public class CardGenerator{
    //main method required for every Java program
    public static void main(String[] args){
      
//assign a variable using the Math class
//this will randomly generate a number from an indicated set, here being from 1 to 52
//note that this method returns a random double with bounds [0,x), so to exclude 0 and include the upper bound, we must add 1
//because it returns a double, we must cast as an integer
int card = (int)(Math.random()* 52)+1;
      //System.out.println(card);
      //this print statement is in case you would like to know what number the computer generated, though it is not explicitly asked for
      
//create a string variable to store the suit of the card
String suit = new String();

//use if statements to group numbers into suits
//1-13 are diamonds, 14-26 are clubs, 27-39 are hearts, and 40-52 are spades
if (card > 0 && card < 14){ //if the card is between 0 and 14 (exclusive), then...
  suit = "Diamonds"; // ...the suit of the card is diamonds
}   

if (card > 13 && card < 27){
  suit = "Clubs";
}      

if (card > 26 && card < 40){
  suit = "Hearts";
}      
      
if (card > 39 && card < 53){
  suit = "Spades";
}
      
  
      
//use a switch statement to print a number and suit for any generated number
//switch statements will translate the numbers into 
//use the modulus function to make it easier to identify the corresponding labels for each number; use 13 because that is the number of cards per suit
//modulus will give you the remainder of a particular division i.e. 52/13 will give a remainder of 0 because 13 goes into 52 an exact number of times
//this way, if the remainder is, say 3, you will know that the card generated must translate to a 3 of some suit, which will have already been determined above
  switch (card % 13) {
    case 0: //if the remainder is 0
      System.out.println("Your card is the King of " + suit); //print the card number and suit
      break; //every switch statement needs a break before moving on to the next case
    case 1:
      System.out.println("Your card is the Ace of " + suit);
      break;
    case 2:
      System.out.println("Your card is the 2 of " + suit);
      break;
    case 3:
      System.out.println("Your card is the 3 of " + suit);
      break;
    case 4:
      System.out.println("Your card is the 4 of " + suit);
      break;
    case 5:
      System.out.println("Your card is the 5 of " + suit);
      break;
    case 6:
      System.out.println("Your card is the 6 of " + suit);
      break;
    case 7:
      System.out.println("Your card is the 7 of " + suit);
      break;
    case 8:
      System.out.println("Your card is the 8 of " + suit);
      break;
    case 9:
      System.out.println("Your card is the 9 of " + suit);
      break;
    case 10:
      System.out.println("Your card is the 10 of " + suit);
      break;  
    case 11:
      System.out.println("Your card is the Jack of " + suit);
      break;  
    case 12:
      System.out.println("Your card is the Queen of " + suit);
      break;  
        
  } //end of switch statement   
    
    } //end of main method
} //end of class
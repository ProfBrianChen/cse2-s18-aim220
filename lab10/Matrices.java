//Ana Isabela Madrigal 
//Friday, April 20, 2018
//CSE 002
//This code is meant to detail how to use double variable arrays


import java.util.Scanner;

public class Matrices{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("Enter the row length of your matrix: ");
    
    int row = myScanner.nextInt();
    
    System.out.print("Enter the column length of your matrix: ");
    
    int column = myScanner.nextInt();
    
    System.out.print("For numbers increasing horizontally, type 1. For numbers increasing vertically, type 2: ");
    
    int orientation = myScanner.nextInt();
    
    boolean rowMajor = true;
    
    if (orientation == 1){
     rowMajor = true;
    }//end if
    
    else if (orientation == 2){
      rowMajor = false;
    }
    
    increasingMatrix(row, column, rowMajor);
   
  }//end main method
  
  
  
  public static void increasingMatrix(int width, int height, boolean format){
    

    
    if (format == true){
      
      int [] [] matrix = new int [width] [height]; 
      
      int content = width * height;
      int counter = 0;
      for (int i= (width * counter); i == height; i++){
        
      for (int j=0; j == content/counter; j++){
        matrix [i][j] = j; 
        System.out.print(matrix[i][j]); 
      
      }//end nested for loop
        
        System.out.println("");
        
    }//end for loop
    }//end if
    
    
    else if (format == false){
      System.out.println("Column Major Chosen");
    }
    
  }//end method increasingMatrix
  
  
  
}//end class
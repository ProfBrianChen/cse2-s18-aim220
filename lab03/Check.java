// Ana Isabela Madrigal
// Friday, February 9, 2018
// CSE002
// This program is meant to receive input from the user to split a bill in an indicated number of ways and add a desired tip percentage


//imports the Scanner class
  import java.util.Scanner;

  public class Check{
    //main method required for every Java program
    public static void main(String[] args){
      
//to enable users to input info, construct an instance of Scanner
//this tells Scanner that your instance will take input from STDIN
Scanner myScanner= new Scanner( System.in);
      
//prompt user for original cost of check
System.out.print("Enter the original cost of the check in the form xx.xx: ");
//accept user input 
double checkCost= myScanner.nextDouble();      
//prompt user for desired tip percentage 
System.out.print("Enter the percentage tip that you wish to pay in the form xx (a whole number): ");    
//accept user input
double tipPercent= myScanner.nextDouble();
//convert percentage to a decimal value
tipPercent /= 100;
//prompt user for number of people splitting the check
System.out.print("Enter the number of ways the check will be split: ");      
//accept user input
int numPeople= myScanner.nextInt();
      
      
//print output
double totalCost;//declare variable
double costPerPerson;//declare variable
int dollars, dimes, pennies; //stores whole dollar amounts and cents
totalCost= checkCost*(1+tipPercent);//total cost of bill with tip      
costPerPerson= totalCost/numPeople;//cost after splitting check an indicated number of ways
dollars=(int)costPerPerson;//puts number into dollars and cents      
dimes=(int)(costPerPerson*10) % 10;
pennies=(int)(costPerPerson*100) % 10;
System.out.println("Each person owes $" + dollars + '.' + dimes + pennies);
    
    
    
    
    
    } //end of main method
  } //end of class
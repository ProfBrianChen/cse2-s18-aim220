import java.util.Scanner;

public class ArraysOfficial{
  
  public static void printRowMajor(int[][] array){
    
    
    for (int i=0; i<array.length; i++){
      
      for(int j = 0; j<array[i].length; j++){
        
        
        System.out.printf("%3i", array[i][j]);
        
      }
      
      System.out.println("");
    }
    
    
    
  }
  
  
  public static void main(String[] args){
    
    int[][] test = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10,}, {11, 12, 13, 14, 15}
                   };
   printRowMajor(test); 
  }
}